/*
I.1.1 Ecrivez une fonction qui prenne en paramètre deux int, et qui renvoie la somme de ces deux entiers.
I.1.2 Ecrivez une autre fonction qui prenne en paramètre trois entiers, et qui remplace la valeur du troisième par la somme des deux premiers. Faites ceci deux fois, une fois à l’aide des pointeurs, une fois à l’aide des références.
I.1.3 Ecrivez un programme générant un tableau d’entiers rempli de valeurs aléatoires toutes positives.
Affichez les valeurs successives du tableau dans la console. En utilisant une des méthodes écrites pour la question 1.2, effectuez un tri du tableau par ordre croissant et affichez le dans la console.
Bonus : Faites en sortes que la taille du tableau soit générée en fonction d’une saisie de l’utilisateur, et que le tri soit croissant ou décroissant en fonction d’une saisie utilisateur également.
Bonus 2 : après avoir effectué un tri (croissant ou décroissant), faites le tri inverse toujours en utilisant la méthode développée en 1.2, en faisant aussi peu d’appels que possible.
*/
#include <iostream>

void Addition(int a, int b) {
    int resultat;
    resultat = a + b;
    std::cout << a << " + " << b << " = " << resultat << std::endl;
}

void Somme_Pointeur(int x, int y, int* z) {
    *z = x + y;
}

void Somme_Reference(int x, int y, int& z) {
    z = x + y;
}

bool Tri_Utilisateur() {
    char reponse;

    do {
        std::cout << "Voulez vous que le tri soit croissant ou decroissant ? [c/d] : ";
        std::cin >> reponse;
    } while (!(reponse == 'c' || reponse == 'd'));

    if (reponse == 'c') {
        return true;
    }

    else if (reponse == 'd') {
        return false;
    }

    else {
        std::cout << "\nLa réponse doit etre 'c' ou 'd'.\n";
    }
}

int Taille_Utilisateur() {
    int valeur;
    std::cout << "Entrez la taille du tableau : ";
    std::cin >> valeur;
    return valeur;
}

void Affiche_Tableau(int tableau[], int taille) {
    for (int i = 0; i < taille; i++) {
        std::cout << tableau[i] << ' ';
    }

    std::cout << std::endl;
}

void Trie_Croissant(int tableau[], int taille) {
    bool verification;

    do {
        verification = false;

        for (int i = 0; i < taille - 1; i++) {
            if (tableau[i] > tableau[i + 1]) {
                int sauvegarde = tableau[i];
                tableau[i] = tableau[i + 1];
                tableau[i + 1] = sauvegarde;

                verification = true;
            }
        }
    } while (verification);
    Affiche_Tableau(tableau, taille);
}

void Trie_Decroissant(int tableau[], int taille) {
    bool verification;

    do {
        verification = false;

        for (int i = 0; i < taille - 1; i++) {
            if (tableau[i] < tableau[i + 1]) {
                int sauvegarde = tableau[i];
                tableau[i] = tableau[i + 1];
                tableau[i + 1] = sauvegarde;

                verification = true;
            }
        }
    } while (verification);
    Affiche_Tableau(tableau, taille);
}

int main()
{
    Addition(4, 5);

    std::cout << std::endl;

    int pt;
    Somme_Pointeur(2, 3, &pt);
    std::cout << pt << std::endl;

    int ref;
    Somme_Reference(45000, 60000, ref);
    std::cout << ref << std::endl;

    std::cout << std::endl;

    int taille = Taille_Utilisateur();
    int* tableau = new int[taille];
    srand(time(NULL));

    for (int i = 0; i < taille; i++) {
        int x = rand() % 100;
        tableau[i] = x;
    }

    std::cout << "Tableau non trie : ";
    Affiche_Tableau(tableau, taille);

    bool reponse = Tri_Utilisateur();

    if (reponse) {
        Trie_Croissant(tableau, taille);
    }

    else {
        Trie_Decroissant(tableau, taille);
    }
}