/*
III.1 Affichage et saisie de chaînes de caractères.
III.1.1 Ecrivez un programme qui demande son prénom à l’utilisateur, puis qu’il salue l’utilisateur.
III.1.2 Modifiez - le ensuite pour qu’il demande le nom ET le prénom(en une seule saisie), et qu’il salue l’utilisateur en réutilisant son nom et son prénom.
Bonus : quel que soit le format de saisie de l’utilisateur, seule la première lettre du prénom doit être en majuscule, toutes les lettres du nom de famille doivent être mises en majuscules.
III.2 Affichage et saisie de nombres.
III.2.1 Écrivez un programme en C++ permettant d’afficher un texte dans la console et de générer un nombre aléatoire entre 0 et 1000. Demandez ensuite à l’utilisateur de saisir un nombre, et affichez un texte qui indique si le nombre saisi était plus grand ou plus petit que le nombre généré aléatoirement.
III.2.2 Modifiez le programme pour que le joueur doive continuer à saisir des nombres jusqu’à ce qu’il ait deviné le nombre généré aléatoirement.Une fois le nombre deviné, un message de félicitations doit s’afficher, avec le nombre d’essais avant de deviner.
Bonus : Ecrivez un programme qui permette à l’utilisateur de choisir un nombre entre 0 et 1000, et qui fasse deviner l’ordinateur en lui disant si le nombre choisi est plus grand ou plus petit que le nombre qu’il a affiché. L’ordinateur devra deviner avec le moins d’essais possible.
*/

#include <iostream>
#include <string>

void Nom_Prenom() {
    std::string nomPrenom;
    std::cout << "Entrez votre nom et prenom : ";
    std::getline(std::cin, nomPrenom);
    std::cout << "Bonjour " << nomPrenom << std::endl;
}

void Nombre_Alleatoire() {
    int nbAlleatoire, nbUtilisateur;

    nbAlleatoire = rand() % 1000;

    do {
        std::cout << "Entrez un nombre entre 0 et 1000 : ";
        std::cin >> nbUtilisateur;

        if (nbUtilisateur < nbAlleatoire) {
            std::cout << "C'est plus" << std::endl;
        }
        else if (nbUtilisateur > nbAlleatoire) {
            std::cout << "C'est moins" << std::endl;
        }
    } while (nbUtilisateur != nbAlleatoire);

    std::cout << "Votre nombre est egal, felicitation." << std::endl;

    std::cout << nbAlleatoire << std::endl;
}

int Deviner_Nombre() {
    int min = 0, max = 1000, essais = 0, nombreDevine, valUtilisateur;

    do {
        std::cout << "Entrez une valeur a faire deviner a l'ordinateur (entre 0 et 1000) : ";
        std::cin >> valUtilisateur;
        if (valUtilisateur > 1000 || valUtilisateur < 0) {
            std::cout << "La valeur n'est pas comprise entre 0 et 1000" << std::endl;
        }
    } while (valUtilisateur > 1000 || valUtilisateur < 0);

    while (true) {
        nombreDevine = (min + max) / 2;
        essais++;

        std::cout << "L'ordinateur devine : " << nombreDevine << std::endl;

        if (nombreDevine == valUtilisateur) {
            std::cout << "L'ordinateur a devine en " << essais << " essais." << std::endl;
            return nombreDevine;
        }

        if (nombreDevine < valUtilisateur) {
            std::cout << "Le nombre est plus grand." << std::endl;
            min = nombreDevine + 1;
        }
        else {
            std::cout << "Le nombre est plus petit." << std::endl;
            max = nombreDevine - 1;
        }
    }
}

int main()
{
    Nom_Prenom();
    Nombre_Alleatoire();
    Deviner_Nombre();
}