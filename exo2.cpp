/*
Ecrivez une fonction qui permette de déterminer le score d’un jeu de tennis, en fonction du nombre d’échanges qui ont été remportés par chaque joueur.
Le système de pointage est assez simple :
1. Chaque joueur peut avoir l'un ou l'autre de ces points dans un jeu 0 15 30 40*
2. Si vous avez 40 et que vous gagnez l’échange, vous gagnez le jeu, mais il existe des règles spéciales.
3. Si les deux ont 40, les joueurs sont à égalité.
a.Si le jeu est à égalité, le gagnant d'une balle aura l'avantage et la balle de jeu.
b.Si le joueur avec l'avantage gagne la balle, il gagne le jeu.
c.Si le joueur sans avantage gagne, ils sont à égalité.
*/

#include <iostream>
#include <string>

std::string Score(int scorej1, int scorej2) {
    std::string score = "";

    std::string tableauscores[] = { "0", "15", "30", "40" };

    if (scorej1 < 3 && scorej2 < 3) {
        score = tableauscores[scorej1] + " - " + tableauscores[scorej2];
    }

    else if (scorej1 >= 3 && scorej2 >= 3) {

        if (scorej1 == scorej2) {
            score = "40 - 40";
        }
        else {
            if (scorej1 > scorej2) {
                score = "Avantage - 40";
            }
            else {
                score = "40 - Avantage";
            }
        }
    }
    else {
        score = tableauscores[scorej1] + " - " + tableauscores[scorej2];
    }

    return score;
}

int main() {
    int scorej1 = 2;
    int scorej2 = 3;

    std::cout << "Score du jeu : " << Score(scorej1, scorej2) << std::endl;

    return 0;
}