#pragma once
#include <string>
#include <cctype>
#include "Joueur.h"

class PuissanceQuatre
{
public:
	PuissanceQuatre();
	void afficherGrille();
	bool verifiePlace(int, Joueur);
	bool verifieGagnant(Joueur);
	bool jouerIA();
private:
	char grilleP4[4][7];
};