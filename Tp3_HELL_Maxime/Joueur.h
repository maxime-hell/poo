#pragma once
#include <string>

class Joueur
{
public:
	Joueur();
	Joueur(std::string, int);
	void Tour();
	void affichePseudo();
	void definiJoueur(std::string, int);
	const std::string getPseudo();
	const int getOrdre();
	const char getSymbole();

private:
	std::string pseudo;
	int ordre;
	int symbole;
	int tours = 1;
};