//Vous avez maintenant un morpion fonctionnel, vous allez maintenant d�velopper un puissance 4. Pour cela, vous allez ajouter des classes permettant de jouer au Puissance 4.
//On ne joue plus sur une grille de 3 par 3, mais sur une grille dont la largeur sera de 7, et la hauteur de 4, soit 4 lignes et 7 colonnes.
//R�gle du puissance 4 : chaque joueur joue alternativement(d�abord le premier, le deuxi�me, ...).Le joueur choisit une colonne libre(non pleine) pour d�poser un de ses jetons.Le jeton occupe la case libre la plus basse dans la colonne.
//Un joueur gagne la partie lorsque qu�il a r�ussi � remplir une ligne, une colonne, ou une diagonale de ces jetons.Contrairement au morpion, on choisit uniquement la colonne dans laquelle on joue.On parcourt ensuite la colonne, le pion est plac� dans la case la plus basse qui soit vide au moment de jouer.
//Contrairement au morpion, le joueur ne gagne pas quand il remplit une ligne, une colonne ou une diagonale, mais quand il aligne 4 pions de sa couleur en ligne en colonne ou en diagonale.
//Vous d�velopperez donc des m�thodes permettent d�afficher la grille, de placer un pion, de v�rifier qu�un joueur a gagn� et d�afficher le r�sultat de la fin de partie.
//Vous modifierez votre classe responsable du d�roulement du jeu pour qu�elle puisse g�rer indiff�remment une partie de morpion ou de puissance 4, et diriger une partie de l�un ou de l�autre

#include <iostream>
#include <cctype>
#include "PuissanceQuatre.h"
#include "Joueur.h"

PuissanceQuatre::PuissanceQuatre() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 7; j++) {
            grilleP4[i][j] = ' ';
        }
    }
}

void PuissanceQuatre::afficherGrille() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 7; j++) {
            std::cout << grilleP4[i][j];
            if (j < 6) {
                std::cout << " | ";
            }
        }
        std::cout << std::endl;
        if (i < 3) {
            std::cout << "-------------------------" << std::endl;
        }
    }
}

bool PuissanceQuatre::jouerIA()
{
    int val;
    do
    {
        std::cout << "Etes vous 1 joueur ou 2 joueurs (1,2) : ";
        std::cin >> val;

        if (val == 1) {
            return true;
        }

        else if (val == 2) {
            return false;
        }

        else {
            std::cout << "Entrez une valeur correcte" << std::endl;
        }

    } while (val != 1 || val != 2);
}

bool PuissanceQuatre::verifiePlace(int _colonne, Joueur _joueur)
{
    if (_colonne <= 0 || _colonne >= 8) {
        std::cout << "La colonne doit etre entre 1 et 7 : ";
        return false;
    }

    if (grilleP4[0][_colonne - 1] != ' ') {
        std::cout << "La colonne est pleine, choissisez en une autre : ";
        return false;
    }

    std::cout << "Place libre, affectation" << std::endl << std::endl;

    for (int i = 3; i >= 0; i--) {
        if (grilleP4[i][_colonne - 1] == ' ') {
            grilleP4[i][_colonne - 1] = _joueur.getSymbole();
            return true;
        }
    }
}

bool PuissanceQuatre::verifieGagnant(Joueur _joueur){

    char symbole = _joueur.getSymbole();
    int lignes = 4;
    int colonnes = 7;

    for (int i = 0; i < lignes; i++) {
        for (int j = 0; j <= colonnes - 4; j++) {
            if (grilleP4[i][j] == symbole &&
                grilleP4[i][j + 1] == symbole &&
                grilleP4[i][j + 2] == symbole &&
                grilleP4[i][j + 3] == symbole) {
                return true;
            }
        }
    }

    for (int j = 0; j < colonnes; j++) {
        for (int i = 0; i <= lignes - 4; i++) {
            if (grilleP4[i][j] == symbole &&
                grilleP4[i + 1][j] == symbole &&
                grilleP4[i + 2][j] == symbole &&
                grilleP4[i + 3][j] == symbole) {
                return true;
            }
        }
    }

    for (int i = 0; i <= lignes - 4; i++) {
        for (int j = 0; j <= colonnes - 4; j++) {
            if (grilleP4[i][j] == symbole &&
                grilleP4[i + 1][j + 1] == symbole &&
                grilleP4[i + 2][j + 2] == symbole &&
                grilleP4[i + 3][j + 3] == symbole) {
                return true;
            }
        }
    }

    for (int i = 0; i <= lignes - 4; i++) {
        for (int j = 3; j < colonnes; j++) {
            if (grilleP4[i][j] == symbole &&
                grilleP4[i + 1][j - 1] == symbole &&
                grilleP4[i + 2][j - 2] == symbole &&
                grilleP4[i + 3][j - 3] == symbole) {
                return true;
            }
        }
    }
    return false;
}