#include <iostream>
#include <string>
#include <time.h>
#include "PuissanceQuatre.h"
#include "Joueur.h"
#include "Morpion.h"

int main()
{
    int jeu;
    std::string pseudo1, pseudo2;
    Joueur joueur1, joueur2;
    const std::string bot = "bot";
    std::cout << "Bienvenu dans le menu de jeu, choissisez votre jeu" << std::endl;

    do {
        std::cout << "1 - Puissance 4\n2 - Morpion" << std::endl;
        std::cin >> jeu;
    } while (jeu != 1 && jeu != 2);

    if (jeu == 1) {
        std::cout << "Jeu choisi : Puissance 4" << std::endl;
    }

    if (jeu == 2) {
        std::cout << "Jeu choisi : Morpion" << std::endl;
    }

    if (jeu == 1) {
        PuissanceQuatre pui4;

        if (pui4.jouerIA()) {
            std::cout << "Entrez le pseudo du joueur qui commencera, il jouera les croix ('x') : ";
            std::cin >> pseudo1;
            joueur1.definiJoueur(pseudo1, 1);
            joueur2.definiJoueur("bot", 2);
            std::cout << "Joueur " << joueur1.getOrdre() << " : " << joueur1.getPseudo() << std::endl;
        }

        else {
            std::cout << "Entrez le pseudo du joueur qui commencera, il jouera les croix ('x') : ";
            std::cin >> pseudo1;
            joueur1.definiJoueur(pseudo1, 1);
            std::cout << "Entrez le pseudo du joueur suivant, il jouera les ronds ('o') : ";
            std::cin >> pseudo2;
            joueur2.definiJoueur(pseudo2, 2);
            std::cout << "Joueur " << joueur1.getOrdre() << " : " << joueur1.getPseudo() << std::endl;
            std::cout << "Joueur " << joueur2.getOrdre() << " : " << joueur2.getPseudo() << std::endl << std::endl;
        }

        pui4.afficherGrille();
        int tour = 0;
        std::cout << "Entrez la colonne : ";
        do{
            int colonne;
            tour++;

            if (tour >= 28) {
                std::cout << std::endl << "Egalite !!!" << std::endl << std::endl;
                pui4.afficherGrille();
                return 0;
            }

            if (tour % 2 != 0) {
                std::cout << "Tour " << tour << ", joueur ";
                joueur1.affichePseudo();
                std::cout << std::endl;
                std::cout << "Choissiez une colonne (1 - 7) : ";
                do {
                    std::cin >> colonne;
                } while (!(pui4.verifiePlace(colonne, joueur1)));

                if (pui4.verifieGagnant(joueur1)) {
                    std::cout << std::endl << "Victoire de " << joueur1.getPseudo() << " !!!" << std::endl;
                    pui4.afficherGrille();
                    return 0;
                }
            }

            else if (joueur2.getPseudo() != bot) {
                std::cout << "Tour " << tour << ", joueur ";
                joueur2.affichePseudo();
                std::cout << std::endl;
                std::cout << "Choissiez une colonne (1 - 7) : ";
                do {
                    std::cin >> colonne;
                } while (!(pui4.verifiePlace(colonne, joueur2)));

                if (pui4.verifieGagnant(joueur2)) {
                    std::cout << std::endl << "Victoire de " << joueur2.getPseudo() << " !!!" << std::endl;
                    pui4.afficherGrille();
                    return 0;
                }
            }

            else if (joueur2.getPseudo() == bot) {
                srand(time(NULL));
                std::cout << "Tour " << tour << ", joueur ";
                joueur2.affichePseudo();
                std::cout << std::endl;
                do {
                    colonne = (rand() % 7) + 1;
                } while (!(pui4.verifiePlace(colonne, joueur2)));

                if (pui4.verifieGagnant(joueur2)) {
                    std::cout << std::endl << "Victoire de " << joueur2.getPseudo() << " !!!" << std::endl;
                    pui4.afficherGrille();
                    return 0;
                }
            }
            pui4.afficherGrille();
        } while (true);
    }

    if (jeu == 2) {
        Morpion mor;

        if (mor.jouerIA()) {
            std::cout << "Entrez le pseudo du joueur qui commencera, il jouera les croix ('x') : ";
            std::cin >> pseudo1;
            joueur1.definiJoueur(pseudo1, 1);
            joueur2.definiJoueur("bot", 2);
            std::cout << "Joueur " << joueur1.getOrdre() << " : " << joueur1.getPseudo() << std::endl;
        }

        else {
            std::cout << "Entrez le pseudo du joueur qui commencera, il jouera les croix ('x') : ";
            std::cin >> pseudo1;
            joueur1.definiJoueur(pseudo1, 1);
            std::cout << "Entrez le pseudo du joueur suivant, il jouera les ronds ('o') : ";
            std::cin >> pseudo2;
            joueur2.definiJoueur(pseudo2, 2);
            std::cout << "Joueur " << joueur1.getOrdre() << " : " << joueur1.getPseudo() << std::endl;
            std::cout << "Joueur " << joueur2.getOrdre() << " : " << joueur2.getPseudo() << std::endl << std::endl;
        }

        mor.afficherGrille();
        int tour = 0;
        do{
            int place;
            tour++;

            if (tour >= 10) {
                std::cout << std::endl << "Egalite !!!" << std::endl << std::endl;
                mor.afficherGrille();
                return 0;
            }

            if (tour % 2 != 0) {
                std::cout << "Tour " << tour << ", joureur ";
                joueur1.affichePseudo();
                std::cout << std::endl;
                std::cout << "Choissiez une case (1 - 9): ";
                do{
                    std::cin >> place;
                } while (!(mor.verifiePlace(place, joueur1)));

                if (mor.verifieGagnant(joueur1)){
                    std::cout << std::endl << "Victoire de " << joueur1.getPseudo() << " !!!" << std::endl;
                    mor.afficherGrille();
                    return 0;
                }
            }

            else if (joueur2.getPseudo() != bot){
                std::cout << "Tour " << tour << ", joureur ";
                joueur2.affichePseudo();
                std::cout << std::endl;
                std::cout << "Choissiez et une case (1 - 9): ";
                do{
                    std::cin >> place;
                } while (!(mor.verifiePlace(place, joueur2)));

                if (mor.verifieGagnant(joueur2)){
                    std::cout << std::endl << "Victoire de " << joueur2.getPseudo() << " !!!" << std::endl;
                    mor.afficherGrille();
                    return 0;
                }
            }

            else if (joueur2.getPseudo() == bot) {
                srand(time(NULL));
                std::cout << "Tour " << tour << ", joureur ";
                joueur2.affichePseudo();
                std::cout << std::endl;
                do
                {
                    place = (rand() % 9) + 1;
                } while (!(mor.verifiePlace(place, joueur2)));

                if (mor.verifieGagnant(joueur2)) {
                    std::cout << std::endl << "Victoire de " << joueur2.getPseudo() << " !!!" << std::endl;
                    mor.afficherGrille();
                    return 0;
                }
            }

            mor.afficherGrille();
        } while (true);
    }
}