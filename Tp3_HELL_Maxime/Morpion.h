#pragma once
#include <string>
#include <cctype>
#include "Joueur.h"

class Morpion
{
public:
    Morpion();
    void afficherGrille();
    bool verifiePlace(int, Joueur);
    bool verifieGagnant(Joueur);
    bool verifieEgalite();
    bool jouerIA();
private:
    char morpion[3][3] = {
        {'1', '2', '3'},
        {'4', '5', '6'},
        {'7', '8', '9'}
    };
};