#include <iostream>
#include "Joueur.h"

Joueur::Joueur() {

}

Joueur::Joueur(std::string _pseudo, int _ordre)
{
    definiJoueur(_pseudo, _ordre);
}

void Joueur::Tour()
{
    std::cout << "Tours " << tours;

    if (tours % 2 != 0) {
        std::cout << " impair " << std::endl;
    }
    else {
        std::cout << " pair" << std::endl;
    }

    tours++;
}

void Joueur::affichePseudo()
{
    std::cout << pseudo;
}

void Joueur::definiJoueur(std::string _pseudo, int _ordre)
{
    pseudo = _pseudo;
    ordre = _ordre;

    if (_ordre == 1) {
        symbole = 'x';
    }
    else {
        symbole = 'o';
    }
}

const std::string Joueur::getPseudo()
{
	return pseudo;
}

const int Joueur::getOrdre()
{
	return ordre;
}

const char Joueur::getSymbole()
{
    return symbole;
}