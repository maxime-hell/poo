//Traditionnellement, un morpion se joue � deux joueurs sur une grille de 3x3. Le plateau de jeu peut �tre mod�lis� par un tableau d�entiers � deux dimensions. En C++, une collection en deux dimensions d�entiers sera souvent une collection contenant une autre collection.
//1. Le joueur dont c�est le tour est invit� � saisir la case sur laquelle il veut poser un pion,
//2. On v�rifie que la valeur saisie est bien un entier compris entre 1 et le nombre de lignes, sinon on redemande de saisir.
//3. On v�rifie que la case s�lectionn�e est vide et dans les bornes du tableau,
//4. Si c�est le cas, on place le pion pour le joueur et on affiche la grille,
//5. On v�rifie si le joueur a gagn�, si oui, la partie est termin�e et on f�licite le vainqueur.
//6. On v�rifie si la grille est pleine, si oui on annonce le match nul et on propose de jouer � nouveau.

#include <iostream>
#include <cctype>
#include "Morpion.h"
#include "Joueur.h"

Morpion::Morpion()
{

}

void Morpion::afficherGrille()
{
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            std::cout << morpion[i][j];
            if (j < 2) {
                std::cout << " | ";
            }
        }
        std::cout << std::endl;
        if (i < 2) {
            std::cout << "---------" << std::endl;
        }
    }
    std::cout << std::endl;
}

bool Morpion::verifiePlace(int _case, Joueur _joueur)
{
    if (_case <= 0 || _case >= 10)
    {
        std::cout << "La case doit etre en 1 et 9 : ";
        return false;
    }

    int x, y;
    switch (_case)
    {
    case 1:
        x = 0;
        y = 0;
        break;
    case 2:
        x = 0;
        y = 1;
        break;
    case 3:
        x = 0;
        y = 2;
        break;
    case 4:
        x = 1;
        y = 0;
        break;
    case 5:
        x = 1;
        y = 1;
        break;
    case 6:
        x = 1;
        y = 2;
        break;
    case 7:
        x = 2;
        y = 0;
        break;
    case 8:
        x = 2;
        y = 1;
        break;
    case 9:
        x = 2;
        y = 2;
        break;
    default:
        break;
    }

    if (morpion[x][y] == 'x' || morpion[x][y] == 'o') {
        std::cout << "La case est prise, choissiez en une autre : ";
        return false;
    }


    std::cout << "Place libre, affectation" << std::endl;
    morpion[x][y] = _joueur.getSymbole();
    return true;
}

bool Morpion::verifieGagnant(Joueur _joueur) {
    char symbole = _joueur.getSymbole();

    for (int i = 0; i < 3; i++) {
        if (morpion[i][0] == symbole && morpion[i][1] == symbole && morpion[i][2] == symbole) {
            return true;
        }
    }

    for (int j = 0; j < 3; j++) {
        if (morpion[0][j] == symbole && morpion[1][j] == symbole && morpion[2][j] == symbole) {
            return true;
        }
    }

    if (morpion[0][0] == symbole && morpion[1][1] == symbole && morpion[2][2] == symbole) {
        return true;
    }

    if (morpion[0][2] == symbole && morpion[1][1] == symbole && morpion[2][0] == symbole) {
        return true;
    }

    return false;   
}

bool Morpion::verifieEgalite()
{
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (std::isdigit(morpion[i][j])) {
                return false;
            }
        }
    }
    return true;
}

bool Morpion::jouerIA()
{
    int val;
    do
    {
        std::cout << "Etes vous 1 joueur ou 2 joueurs (1,2) : ";
        std::cin >> val;

        if (val == 1) {
            return true;
        }

        else if (val == 2) {
            return false;
        }

        else {
            std::cout << "Entrez une valeur correcte" << std::endl;
        }

    } while (val != 1 || val != 2);
}
