#include "Triangle.h"
#include "Point.h"

Triangle::Triangle(const Point p1, const Point p2, const Point p3)
{
	setPoint1(p1);
	setPoint2(p2);
	setPoint3(p3);
}

Point Triangle::getPoint1() const
{
	return Point1;
}

Point Triangle::getPoint2() const
{
	return Point2;
}

Point Triangle::getPoint3() const
{
	return Point3;
}

void Triangle::setPoint1(const Point p)
{
	Point1 = p;
}

void Triangle::setPoint2(const Point p)
{
	Point2 = p;
}

void Triangle::setPoint3(const Point p)
{
	Point3 = p;
}

double Triangle::calculerBase() const
{
	if (Cote1 >= Cote2 && Cote1 >= Cote3)
	{
		return Cote1;
	}
	if (Cote2 >= Cote1 && Cote2 >= Cote3)
	{
		return Cote2;
	}
	if (Cote3 >= Cote2 && Cote3 >= Cote1)
	{
		return Cote3;
	}
	return Cote1;
}

double Triangle::calculerHauteur() const
{
	double demiPreimetre = (Cote1 + Cote2 + Cote3) / 2;
	double aire = sqrt(demiPreimetre * (demiPreimetre - Cote1) * (demiPreimetre - Cote2) * (demiPreimetre - Cote3));
	double hauteur = (2 * aire) / Cote1;
	return hauteur;
}

double Triangle::calculerSurface() const
{
	return (calculerBase() * calculerHauteur()) / 2.0;
}

bool Triangle::estIsocele() const
{
	return (Cote1 == Cote2 || Cote2 == Cote3 || Cote3 == Cote1);
}

bool Triangle::estRectangle() const
{
	return false;
}

bool Triangle::estEquilateral() const
{
	return Cote1 == Cote2 && Cote2 && Cote3;
}
