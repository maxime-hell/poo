#pragma once
#include "Point.h"

class Rectangle
{
private:
	int Longueur;
	int Largeur;
	Point coinSuperieurGauche;
public:
	Rectangle(int longueur, int largeur, const Point coinSupGauche);
	int getLongueur() const;
	int getLargeur() const;
	Point getPoint() const;
	void setLongueur(int const longueur);
	void setLargeur(int const largeur);
	void setPoint(Point point);

	int Perimetre() const;
	int Surface() const;

};

