#pragma once
#include "Point.h"
#include <math.h>

class Triangle
{
private:
	Point Point1;
	Point Point2;
	Point Point3;
	double Cote1 = sqrt(pow(Point3.x - Point1.x, 2) + pow(Point3.y - Point1.y, 2));
	double Cote2 = sqrt(pow(Point2.x - Point1.x, 2) + pow(Point2.y - Point1.y, 2));
	double Cote3 = sqrt(pow(Point3.x - Point2.x, 2) + pow(Point3.y - Point2.y, 2));

public:
	Triangle(const Point p1, const Point p2, const Point p3);

	Point getPoint1() const;
	Point getPoint2() const;
	Point getPoint3() const;
	void setPoint1(const Point p);
	void setPoint2(const Point p);
	void setPoint3(const Point p);


	double calculerBase() const;
	double calculerHauteur() const;
	double calculerSurface() const;
	bool estIsocele() const;
	bool estRectangle() const;
	bool estEquilateral() const;
};

