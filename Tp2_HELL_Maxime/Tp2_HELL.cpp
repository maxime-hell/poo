#include <iostream>
#include <cmath>
#include "Rectangle.h"
#include "Point.h"
#include "Cercle.h"
#include "Triangle.h"
#include "Carre.h"

int main() {
    //--------
	Point csg = { 1,2 };
	const Rectangle monRectangle(4, 3, csg);

    std::cout << "Informations sur le rectangle :" << std::endl;
    std::cout << "Longueur : " << monRectangle.getLongueur() << std::endl;
    std::cout << "Largeur : " << monRectangle.getLargeur() << std::endl;

    const Point coinSupGaucheResult = monRectangle.getPoint();
    std::cout << "Coin superieur gauche : (" << coinSupGaucheResult.x << ", " << coinSupGaucheResult.y << ")" << std::endl;

    std::cout << "Perimetre : " << monRectangle.Perimetre() << std::endl;
    std::cout << "Surface : " << monRectangle.Surface() << std::endl;

    //--------
    Point centreC = { 0, 0 };
    Cercle monCercle(centreC, 10);

    std::cout << "\nInformations sur le cercle :" << std::endl;
    std::cout << "Centre : (" << monCercle.getCentre().x << ", " << monCercle.getCentre().y << ")" << std::endl;
    std::cout << "Diametre : " << monCercle.getDiametre() << std::endl;
    std::cout << "Perimetre : " << monCercle.calculerPerimetre() << std::endl;
    std::cout << "Surface : " << monCercle.calculerSurface() << std::endl;

    Point pointSurLeCercle = { 5, 0 };
    if (monCercle.estSurLeCercle(pointSurLeCercle)) {
        std::cout << "Le point est sur le cercle." << std::endl;
    }
    else {
        std::cout << "Le point n'est pas sur le cercle." << std::endl;
    }

    Point pointAInterieur = { 2, 0 };
    if (monCercle.estAInterieurDuCercle(pointAInterieur)) {
        std::cout << "Le point est a l'interieur du cercle (mais pas sur le cercle)." << std::endl;
    }
    else {
        std::cout << "Le point n'est pas a l'interieur du cercle (ou il est sur le cercle)." << std::endl;
    }

    //--------
    Point Tri1 = { 0, 0 };
    Point Tri2 = { 5, 3 };
    Point Tri3 = { 10, 6 };
    Triangle monTriangle(Tri1, Tri2, Tri3);

    std::cout << "\nInformations sur le triangle :" << std::endl;
    std::cout << "Point 1 : " << Tri1.x << ", " << Tri1.y << std::endl;
    std::cout << "Point 2 : " << Tri2.x << ", " << Tri2.y << std::endl;
    std::cout << "Point 3 : " << Tri3.x << ", " << Tri3.y << std::endl;
    std::cout << "Base : " << monTriangle.calculerBase() << std::endl;
    std::cout << "Hauteur : " << monTriangle.calculerHauteur() << std::endl;
    std::cout << "Surface : " << monTriangle.calculerSurface() << std::endl;
    std::cout << "Est equilaterl ? : " << monTriangle.estEquilateral() << " (0 = Non, 1 = Oui)" << std::endl;
    std::cout << "Est isocele ? : " << monTriangle.estIsocele() << " (0 = Non, 1 = Oui)" << std::endl;


    //--------
    Carre monCarre(5, csg);
    std::cout << "\nInformations sur le carre :" << std::endl;
    std::cout << "Longueur : " << monCarre.getLongueur() << std::endl;
    std::cout << "Largeur : " << monCarre.getLargeur() << std::endl;
    const Point coinSupGaucheCarre = monCarre.getPoint();
    std::cout << "Coin superieur gauche : (" << coinSupGaucheCarre.x << ", " << coinSupGaucheCarre.y << ")" << std::endl;

    std::cout << "Perimetre : " << monCarre.Perimetre() << std::endl;
    std::cout << "Surface : " << monCarre.Surface() << std::endl;
}