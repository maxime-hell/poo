#pragma once
#include "Point.h"

class Cercle {
private:
    Point Centre;
    int Diametre;
    int const pi = 3.1415926535;

public:
    Cercle(const Point centre, int diametre);
    Point getCentre() const;
    int getDiametre() const;
    void setCentre(const Point nouveauCentre);
    void setDiametre(int nouveauDiametre);

    double calculerPerimetre() const;
    double calculerSurface() const;
    bool estSurLeCercle(const Point point) const;
    bool estAInterieurDuCercle(const Point point) const;
};


