#include "Cercle.h"
#include "Point.h"
#include <math.h>

Cercle::Cercle(const Point centre, int diametre)
{
	setCentre(centre);
	setDiametre(diametre);
}

Point Cercle::getCentre() const
{
	return Centre;
}

int Cercle::getDiametre() const
{
	return Diametre;
}

void Cercle::setCentre(const Point nouveauCentre)
{
	Centre = nouveauCentre;
}

void Cercle::setDiametre(int nouveauDiametre)
{
	Diametre = nouveauDiametre;
}

double Cercle::calculerPerimetre() const
{
	return pi * Diametre;
}

double Cercle::calculerSurface() const
{
	return pi * pow(Diametre /2.0, 2);
}

bool Cercle::estSurLeCercle(const Point point) const
{
	double distCentre = sqrt(pow(point.x - Centre.x, 2) + pow(point.y - Centre.y, 2));
	return (distCentre == (Diametre / 2));
}

bool Cercle::estAInterieurDuCercle(const Point point) const
{
	double distCentre = sqrt(pow(point.x - Centre.x, 2) + pow(point.y - Centre.y, 2));
	return distCentre < (Diametre / 2);
}

