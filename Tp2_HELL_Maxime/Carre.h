#pragma once
#include "Point.h"
#include "Rectangle.h"

class Carre : public Rectangle {
public:
	Carre(int cote, Point point);
	int obtenirLongueurCote() const;

};

