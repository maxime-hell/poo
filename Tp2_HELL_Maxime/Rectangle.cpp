#include "Rectangle.h"
#include "Point.h"

Rectangle::Rectangle(int longueur, int largeur, const Point coinSupGauche)
{
	setLongueur(longueur);
	setLargeur(largeur);
	setPoint(coinSupGauche);
}

int Rectangle::getLongueur() const
{
	return Longueur;
}

int Rectangle::getLargeur() const
{
	return Largeur;
}

Point Rectangle::getPoint() const
{
	return coinSuperieurGauche;
}

void Rectangle::setLongueur(int const longueur)
{
	Longueur = longueur;
}

void Rectangle::setLargeur(int const largeur)
{
	Largeur = largeur;
}

void Rectangle::setPoint(Point point)
{
	coinSuperieurGauche = point;
}

int Rectangle::Perimetre() const
{
	return 2 * (Largeur + Longueur);
}

int Rectangle::Surface() const
{
	return Largeur * Largeur;
}
